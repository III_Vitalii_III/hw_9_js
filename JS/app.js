/* 
1. DOM - це програмний інтерфейс, що дозволяє програмним мовам, таким як JavaScript, звертатися до веб-сторінок і змінювати їхню структуру і зміст. 

2. `innerHTML` дозволяє отримувати або змінювати HTML-вміст елемента, включаючи теги. `innerText` працює з текстовим вмістом елемента, ігноруючи HTML-теги.

3. Для звернення до елемента сторінки за допомогою JavaScript можна використовувати методи `document.getElementById`, `document.querySelector` або `document.querySelectorAll`. Найкращим способом є `querySelector` за його гнучкість і можливість вибору елементів за CSS-селекторами.

4. `NodeList` - це колекція вузлів, що повертається методами як `querySelectorAll`. `HTMLCollection` - це підтип `NodeList`, який містить лише елементи HTML-сторінки, і використовується, наприклад, для доступу до дитячих елементів конкретного вузла DOM, такого як `children` в DOM API.
*/

// Задвання 1

const firstMetod = document.querySelectorAll(".feature");
firstMetod.forEach(element => {
    element.style.textAlign = 'center';
    console.log(firstMetod);
});

const secondMetod = document.getElementsByClassName("feature");
Array.from(secondMetod).forEach( element => {
    element.style.textAlign = 'center';
    console.log(element.secondMetod);
});

// Задвання 2
const textH = document.querySelectorAll('h2');
textH.forEach( element => 
    {
        element.textContent = 'Awesome feature';
    }
);


// Задвання 3
const featureTitles = document.querySelectorAll('.feature-title');
featureTitles.forEach(element => {
    element.textContent = element.textContent + '!';
});
